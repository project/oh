<?php

declare(strict_types=1);

namespace Drupal\oh_test;

use Drupal\oh\Event\OhEvent;
use Drupal\oh\Event\OhEvents;
use Drupal\oh\OhDateRange;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber for OH events.
 */
final class OhTestEventSubscriber implements EventSubscriberInterface {

  /**
   * Scenarios to run.
   *
   * @var string[]
   */
  protected array $scenarios = [];

  /**
   * Allow tests to set scenarios remotely.
   *
   * @param array $scenarios
   *   A list of scenarios.
   */
  public function setScenarios(array $scenarios): void {
    $this->scenarios = $scenarios;
  }

  /**
   * Test events.
   *
   * @param \Drupal\oh\Event\OhEvent $event
   *   Event event.
   */
  public function myEvents(OhEvent $event): void {
    if (\in_array('test_events', $this->scenarios, TRUE)) {
      $event->addEvents(
        new OhDateRange(
          new \DateTimeImmutable('10am 10 October 2018'),
          new \DateTimeImmutable('11am 10 October 2018'),
        ),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[OhEvents::EVENT][] = ['myEvents'];
    return $events;
  }

}
