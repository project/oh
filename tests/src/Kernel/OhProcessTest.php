<?php

declare(strict_types=1);

namespace Drupal\Tests\oh\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\oh\OhDateRange;
use Drupal\oh\OhOpeningHoursInterface;

/**
 * Tests process event.
 *
 * @group oh
 * @coversDefaultClass \Drupal\oh\EventSubscriber\OhEventSubscriber
 */
final class OhProcessTest extends KernelTestBase {

  use OhTestTrait;

  protected static $modules = ['entity_test', 'user', 'oh_test', 'oh'];

  /**
   * Test partial day messages removed when a full day intersects.
   */
  public function testPartialDayMessagesRemoved(): void {
    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
    ]);

    $start = new \DateTime('9 Feb 1998 00:00');
    $end = new \DateTime('16 Feb 1998 00:00');
    $range = new OhDateRange($start, $end);

    // Presetup regression test, make sure partial messages still exist.
    $this->setRegularScenarios(['Open 9-5 13 February 1998 Singapore']);
    $occurrences = $this->openingHoursService()->getOccurrences($entity, $range);
    $this->assertEquals([
      'open 9-5 #1',
      'open 9-5 #2',
    ], $occurrences[0]->getMessages());

    $this->setRegularScenarios(['All day 13 February 1998 Singapore', 'Open 9-5 13 February 1998 Singapore']);
    $occurrences = $this->openingHoursService()->getOccurrences($entity, $range);
    $this->assertEquals([
      // Ensures the partial day exceptions are absorbed.
      'open all day #1',
      'open all day #2',
    ], $occurrences[0]->getMessages());
  }

  /**
   * Opening hours service.
   *
   * @return \Drupal\oh\OhOpeningHoursInterface
   *   The opening hours service.
   */
  protected function openingHoursService(): OhOpeningHoursInterface {
    return $this->container->get('oh.opening_hours');
  }

}
