<?php

declare(strict_types=1);

namespace Drupal\date_recur_oh_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validates OH status.
 *
 * @Constraint(
 *   id = \Drupal\date_recur_oh_field\Plugin\Validation\Constraint\DateRecurOhStatusConstraint::PLUGIN_ID,
 *   label = @Translation("Validates status is set when other values are set.", context = "Validation"),
 * )
 */
class DateRecurOhStatusConstraint extends Constraint {

  public const PLUGIN_ID = 'DateRecurOhStatus';

  /**
   * Violation message for field values without a status.
   *
   * @var string
   */
  public $statusRequired = 'Status field is required.';

}
