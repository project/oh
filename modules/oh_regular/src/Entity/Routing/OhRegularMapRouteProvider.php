<?php

declare(strict_types=1);

namespace Drupal\oh_regular\Entity\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Route provider for Oh Map entities.
 */
class OhRegularMapRouteProvider extends AdminHtmlRouteProvider {

}
