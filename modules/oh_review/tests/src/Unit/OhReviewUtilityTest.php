<?php

declare(strict_types=1);

namespace Drupal\Tests\oh_review\Unit;

use Drupal\oh\OhDateRange;
use Drupal\oh_review\OhReviewUtility;
use Drupal\Tests\UnitTestCase;

/**
 * Tests OhReviewUtility class.
 *
 * @group oh_review
 * @coversDefaultClass \Drupal\oh_review\OhReviewUtility
 */
final class OhReviewUtilityTest extends UnitTestCase {

  /**
   * Tests all occurrences for a week that starting last year are grouped there.
   *
   * @covers ::occurrencesByWeek
   */
  public function testOccurrencesByWeekCrossYear(): void {
    $range = new OhDateRange(
      new \DateTime('9am 21 December 2020'),
      new \DateTime('9pm 15 January 2021'),
    );
    $occurrences = [];
    $result = OhReviewUtility::occurrencesByWeek($range, $occurrences, TRUE);
    $this->assertEquals([
      '2020-52' => [
        '2020-12-21' => [],
        '2020-12-22' => [],
        '2020-12-23' => [],
        '2020-12-24' => [],
        '2020-12-25' => [],
        '2020-12-26' => [],
        '2020-12-27' => [],
      ],
      '2020-53' => [
        '2020-12-28' => [],
        '2020-12-29' => [],
        '2020-12-30' => [],
        '2020-12-31' => [],
        // These dates in 2021 need to appear in the 53rd week of 2020 per
        // ISO-8601 week-numbering year.
        '2021-01-01' => [],
        '2021-01-02' => [],
        '2021-01-03' => [],
      ],
      // This is the first [full] week of the year.
      '2021-01' => [
        '2021-01-04' => [],
        '2021-01-05' => [],
        '2021-01-06' => [],
        '2021-01-07' => [],
        '2021-01-08' => [],
        '2021-01-09' => [],
        '2021-01-10' => [],
      ],
      '2021-02' => [
        '2021-01-11' => [],
        '2021-01-12' => [],
        '2021-01-13' => [],
        '2021-01-14' => [],
        '2021-01-15' => [],
      ],
    ], $result);
  }

}
