<?php

declare(strict_types=1);

namespace Drupal\oh_review\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\oh\OhDateRange;
use Drupal\oh\OhOpeningHoursInterface;
use Drupal\oh_regular\OhRegularInterface;
use Drupal\oh_review\OhReviewUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a report of opening hours for relevant locations.
 */
final class OhReviewReport extends ControllerBase {

  /**
   * Tag to alter the displayed entities.
   */
  const QUERY_TAG_ENTITIES = 'oh_review_report_entities';

  private function __construct(
    protected OhOpeningHoursInterface $openingHours,
    protected OhRegularInterface $ohRegular,
    protected RendererInterface $renderer,
  ) {
  }

  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('oh.opening_hours'),
      $container->get('oh_regular.mapping'),
      $container->get('renderer'),
    );
  }

  /**
   * Creates a report of opening hours for relevant locations.
   *
   * @return array
   *   A render array.
   */
  public function report(): array {
    $cachability = new CacheableMetadata();

    $weeks = 6;

    $build = [];

    $rangeStart = $this->getRangeStart();
    $rangeEnd = (clone $rangeStart)
      ->add(new \DateInterval('P' . $weeks . 'W'));
    $range = new OhDateRange($rangeStart, $rangeEnd);

    // Add a weeks header.
    $header = [
      $this->t('Location'),
      $this->t('Operations'),
    ];
    $weekHeaderStart = clone $rangeStart;
    for ($i = 0; $i < $weeks; $i++) {
      $header[]['data']['#plain_text'] = $this->t('Week of @day', [
        '@day' => $weekHeaderStart->format('jS M Y'),
      ]);
      $weekHeaderStart->modify('+1 week');
    }

    $rows = [];
    foreach ($this->loadEntities() as $entity) {
      $cachability->addCacheableDependency($entity);

      $row = [];
      $row['link'] = $entity->toLink();

      // Use the same method as views' operations field plugin.
      $operations = $this->entityTypeManager()
        ->getListBuilder($entity->getEntityTypeId())
        ->getOperations($entity);
      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      $occurrences = $this->openingHours->getOccurrences($entity, $range);
      foreach ($occurrences as $occurrence) {
        $cachability->addCacheableDependency($occurrence);
      }

      $occurrencesByWeek = OhReviewUtility::occurrencesByWeek($range, $occurrences, TRUE);

      $weekStart = clone $rangeStart;
      foreach ($occurrencesByWeek as $days) {
        $weekEnd = (clone $weekStart)->modify('+1 week');
        $weekEnd->modify('-1 second');
        $weekRange = new OhDateRange($weekStart, $weekEnd);

        $occurrences = \array_merge(...\array_values($days));

        $element = [
          '#theme' => 'oh_review_report_list',
          '#range' => $weekRange,
          '#occurrences' => $occurrences,
        ];
        // Render now since keeping $occurrences around can eventually consume
        // a lot of memory.
        $row[]['data'] = $this->renderer->render($element);
        $weekStart->modify('+1 week');
      }

      $rows[] = $row;

      // GC everything since memory is precious for this report.
      unset($entity);
      unset($occurrences);
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No entities found.'),
      '#sticky' => TRUE,
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    $cachability->applyTo($build);
    $build['#cache']['keys'][] = 'oh_review_report';
    return $build;
  }

  /**
   * Loads entities progressively.
   *
   * @return \Generator<\Drupal\Core\Entity\EntityInterface>
   *   Generates entities.
   */
  protected function loadEntities(): \Generator {
    $allMapping = $this->ohRegular->getAllMapping();
    foreach ($allMapping as $entityTypeId => $mapping) {
      $storage = $this->entityTypeManager()
        ->getStorage($entityTypeId);

      $query = $storage->getQuery()->accessCheck(TRUE);
      $query->pager(50);
      $entityTypeDefinition = $this->entityTypeManager()
        ->getDefinition($entityTypeId);

      $labelKey = $entityTypeDefinition->getKey('label');
      if ($labelKey) {
        $query->sort($labelKey, 'ASC');
      }

      // Check if this entity supports bundles.
      $bundlesKey = $entityTypeDefinition->getKey('bundle');
      if ($bundlesKey) {
        $bundles = \array_keys($mapping);
        $query->condition($bundlesKey, $bundles, 'IN');
      }

      $query->addTag(static::QUERY_TAG_ENTITIES);

      $entityIds = $query->execute();
      foreach (\array_chunk($entityIds, 16) as $idChunk) {
        foreach ($storage->loadMultiple($idChunk) as $entity) {
          yield $entity;
        }
        $storage->resetCache($idChunk);
      }
    }

    return NULL;
  }

  /**
   * Get the range start date.
   *
   * @return \DateTime
   *   The range start date.
   */
  protected function getRangeStart(): \DateTime {
    $dayMap = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];

    // Weekday int. 0-6 (Sun-Sat).
    $firstDayInt = $this->config('system.date')
      ->get('first_day');
    $firstDayStr = $dayMap[$firstDayInt];
    // Today day int.
    $today = (new \DateTime())->format('w');
    $weekStartString = ($today == $firstDayInt ? '' : 'last ') . $firstDayStr . ' 00:00';
    return new \DateTime($weekStartString);
  }

}
