<?php

declare(strict_types=1);

namespace Drupal\oh_review;

use Drupal\oh\OhDateRange;
use Drupal\oh\OhOccurrence;

/**
 * Utility for OH Review.
 */
final class OhReviewUtility {

  /**
   * Groups occurrences into days and weeks.
   *
   * @param \Drupal\oh\OhDateRange $range
   *   The full occurrence range.
   * @param \Drupal\oh\OhOccurrence[] $occurrences
   *   An array of occurrences.
   * @param bool $fillDays
   *   Whether create empty days for when there are no occurrences.
   *
   * @return array<string, array<string, array<\Drupal\oh\OhOccurrence>>>
   *   An array of occurrences grouped by weeks, then days.
   */
  public static function occurrencesByWeek(OhDateRange $range, array $occurrences, bool $fillDays): array {
    $weekFormat = 'o-W';
    $dayFormat = 'Y-m-d';

    /** @var array<string, \Drupal\oh\OhOccurrence[]> $occurrencesByDay */
    $occurrencesByDay = [];

    if ($fillDays) {
      // Fill in the days.
      $fillPointer = $range->period()->getStartDate();
      $fillEnd = $range->period()->getEndDate();
      $fillEnd = $fillEnd->modify('-1 second');
      while ($fillPointer < $fillEnd) {
        $pointerDay = $fillPointer->format($dayFormat);
        if (!isset($occurrencesByDay[$pointerDay])) {
          $occurrencesByDay[$pointerDay] = [];
        }
        $fillPointer = $fillPointer->modify('+1 day');
      }
    }

    \uasort($occurrences, [OhOccurrence::class, 'sort']);
    foreach ($occurrences as $occurrence) {
      $day = $occurrence->period()->getStartDate()->format($dayFormat);
      $occurrencesByDay[$day][] = $occurrence;
    }

    // Group into weeks.
    $groupedByWeek = [];

    foreach ($occurrencesByDay as $dayCode => $occurrences) {
      if (!\count($occurrences)) {
        $day = \DateTime::createFromFormat($dayFormat, $dayCode);
        $week = $day->format($weekFormat);
        $groupedByWeek[$week][$dayCode] = $groupedByWeek[$week][$dayCode] ?? [];
      }
      foreach ($occurrences as $occurrence) {
        $week = $occurrence->period()->getStartDate()->format($weekFormat);
        $day = $occurrence->period()->getStartDate()->format($dayFormat);
        $groupedByWeek[$week][$day][] = $occurrence;
      }
    }

    return $groupedByWeek;
  }

}
