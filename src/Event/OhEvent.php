<?php

declare(strict_types=1);

namespace Drupal\oh\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\oh\OhDateRange;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Used to add events between a date range.
 *
 * These events do not impact opening or closing of an event.
 *
 * @see \Drupal\oh\Event\OhEvents::SPECIAL_EVENT
 */
class OhEvent extends Event {

  /**
   * The events for a location.
   *
   * @var \Drupal\oh\OhDateRange[]
   */
  protected array $events = [];

  /**
   * Construct a new OhRegularEvent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The location.
   * @param \Drupal\oh\OhDateRange $range
   *   Get events between a range of dates.
   */
  public function __construct(
    protected EntityInterface $entity,
    protected OhDateRange $range,
  ) {
  }

  /**
   * Get the location.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The location.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Get the date range to get events between.
   *
   * @return \Drupal\oh\OhDateRange
   *   The date range to get events between.
   */
  public function getRange(): OhDateRange {
    return $this->range;
  }

  /**
   * Add events for a location.
   *
   * @param \Drupal\oh\OhDateRange ...$events
   *   Events for a location.
   *
   * @return $this
   *   Return object for chaining.
   */
  public function addEvents(OhDateRange ...$events) {
    foreach ($events as $occurrence) {
      if ($this->range->isWithin($occurrence)) {
        $this->events[] = $occurrence;
      }
    }
    return $this;
  }

  /**
   * Get the events for a location.
   *
   * @return \Drupal\oh\OhDateRange[]
   *   The events for a location.
   */
  public function getEvents(): array {
    return $this->events;
  }

}
