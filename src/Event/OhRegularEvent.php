<?php

declare(strict_types=1);

namespace Drupal\oh\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\oh\OhDateRange;
use Drupal\oh\OhOccurrence;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Used to add regular hours between a date range.
 *
 * @see \Drupal\oh\Event\OhEvents::REGULAR
 */
class OhRegularEvent extends Event {

  /**
   * The regular hours for a location.
   *
   * @var \Drupal\oh\OhOccurrence[]
   */
  protected array $regularHours = [];

  /**
   * Construct a new OhRegularEvent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The location.
   * @param \Drupal\oh\OhDateRange $range
   *   Get regular hours between a range of dates.
   */
  public function __construct(
    protected EntityInterface $entity,
    protected OhDateRange $range,
  ) {}

  /**
   * Get the location.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Get the date range to get regular hours between.
   */
  public function getRange(): OhDateRange {
    return $this->range;
  }

  /**
   * Add regular hours for a location.
   *
   * @param \Drupal\oh\OhOccurrence $occurrence
   *   Regular hours for a location.
   *
   * @return $this
   *   Return object for chaining.
   *
   * @throws \Exception
   *   If the regular hours is outside of the requested range.
   */
  public function addRegularHours(OhOccurrence $occurrence) {
    if ($this->range->isWithin($occurrence)) {
      $this->regularHours[] = $occurrence;
    }
    else {
      throw new \Exception('Occurrence does not intersect with range.');
    }
    return $this;
  }

  /**
   * Get the regular hours for a location.
   *
   * @return \Drupal\oh\OhOccurrence[]
   *   The regular hours for a location.
   */
  public function getRegularHours(): array {
    return $this->regularHours;
  }

}
