<?php

declare(strict_types=1);

namespace Drupal\oh\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\oh\OhDateRange;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Used to process opening hours after they have been computed.
 *
 * @see \Drupal\oh\Event\OhEvents::PROCESS
 */
class OhProcessEvent extends Event {

  /**
   * Construct a new OhProcessEvent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The location.
   * @param \Drupal\oh\OhDateRange $range
   *   The date range of hours.
   * @param \Drupal\oh\OhOccurrence[] $occurrences
   *   Occurrences to process.
   */
  public function __construct(
    protected EntityInterface $entity,
    protected OhDateRange $range,
    protected array $occurrences,
  ) {
  }

  /**
   * Get the location.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The location.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Get the date range of hours.
   *
   * @return \Drupal\oh\OhDateRange
   *   The date range of hours.
   */
  public function getRange(): OhDateRange {
    return $this->range;
  }

  /**
   * Get occurrences.
   *
   * @return \Drupal\oh\OhOccurrence[]
   *   Occurrences to process.
   */
  public function getOccurrences(): array {
    return $this->occurrences;
  }

  /**
   * Set occurrences.
   *
   * @param \Drupal\oh\OhOccurrence[] $occurrences
   *   Occurrences to process.
   */
  public function setOccurrences(array $occurrences): void {
    $this->occurrences = $occurrences;
  }

}
