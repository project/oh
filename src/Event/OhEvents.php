<?php

declare(strict_types=1);

namespace Drupal\oh\Event;

/**
 * Defines events for opening hours.
 */
final class OhEvents {

  /**
   * Used to add regular hours between a date range.
   *
   * @Event
   *
   * @see \Drupal\oh\Event\OhRegularEvent
   */
  public const REGULAR = 'oh.regular';

  /**
   * Used to add exceptions between a date range.
   *
   * @Event
   *
   * @see \Drupal\oh\Event\OhExceptionEvent
   */
  public const EXCEPTIONS = 'oh.exceptions';

  /**
   * Used to add events between a date range.
   *
   * @Event
   *
   * @see \Drupal\oh\Event\OhEvent
   */
  public const EVENT = 'oh.special_event';

  /**
   * Used to process hours.
   *
   * @Event
   *
   * @see \Drupal\oh\Event\OhProcessEvent
   */
  public const PROCESS = 'oh.process';

}
