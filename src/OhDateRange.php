<?php

declare(strict_types=1);

namespace Drupal\oh;

use League\Period\Period;

/**
 * Defines a date range.
 */
class OhDateRange {

  protected Period $period;

  /**
   * Constructs a new OhDateRange.
   *
   * @param \DateTimeInterface $start
   *   The start date.
   * @param \DateTimeInterface $end
   *   The end date.
   *
   * @throws \InvalidArgumentException
   *   When there is a problem with the start and/or end date.
   */
  public function __construct(
    \DateTimeInterface $start,
    \DateTimeInterface $end,
  ) {
    $this->period = new Period($start, $end);
  }

  /**
   * Creates a range from a period.
   */
  public static function fromPeriod(Period $period): static {
    return new static($period->getStartDate(), $period->getEndDate());
  }

  /**
   * Get the period.
   */
  public function period(): Period {
    return $this->period;
  }

  /**
   * Set the period.
   *
   * @return $this
   *   The period.
   */
  public function setPeriod(Period $period) {
    $this->period = $period;

    return $this;
  }

  /**
   * Get the start date.
   *
   * @deprecated Use period()->getStartDate() instead.
   */
  public function getStart(): \DateTimeImmutable {
    return $this->period->getStartDate();
  }

  /**
   * Set the start date.
   *
   * @param \DateTimeInterface $start
   *   The start date.
   *
   * @return $this
   *   Return object for chaining.
   *
   * @throws \InvalidArgumentException
   *   When there is a problem with the start and/or end date.
   *
   * @deprecated Use setPeriod() instead.
   */
  public function setStart(\DateTimeInterface $start) {
    $this->period = new Period($start, $this->period->getEndDate());

    return $this;
  }

  /**
   * Get the end date.
   *
   * @deprecated Use period()->getEndDate() instead.
   */
  public function getEnd(): \DateTimeImmutable {
    return $this->period->getEndDate();
  }

  /**
   * Set the end date.
   *
   * @param \DateTimeInterface $end
   *   The end date.
   *
   * @return $this
   *   Return object for chaining.
   *
   * @throws \InvalidArgumentException
   *   When there is a problem with the start and/or end date.
   *
   * @deprecated Use setPeriod() instead.
   */
  public function setEnd(\DateTimeInterface $end) {
    $this->period = new Period($this->period->getStartDate(), $end);
    return $this;
  }

  /**
   * Calculates the difference between the start and end dates.
   *
   * @return \DateInterval
   *   The difference between the start and end dates.
   */
  public function diff(): \DateInterval {
    return $this->period->dateInterval();
  }

  /**
   * Callback for \usort()/\uasort() to sort date range objects by start time.
   *
   * @param \Drupal\oh\OhDateRange $a
   *   A date range object.
   * @param \Drupal\oh\OhDateRange $b
   *   A date range object.
   *
   * @return int
   *   Whether date range A is lower than date range B.
   */
  public static function sort(OhDateRange $a, OhDateRange $b): int {
    return $a->period()->getStartDate() <=> $b->period->getStartDate();
  }

  /**
   * Ensures a date range occurs within this date range.
   *
   * @param \Drupal\oh\OhDateRange $innerRange
   *   The inner date range.
   * @param bool $partial
   *   Changes the mode so inner values must intersect outer values in any way.
   *
   * @return true
   *   Returns true if inner range is within outer range. Exception otherwise.
   *
   * @throws \Exception
   *   Thrown if this date range exceeds the boundaries of the outer date range.
   */
  public function isWithin(OhDateRange $innerRange, bool $partial = FALSE): true {
    return $partial
      ? ($this->period->overlaps($innerRange->period) ?: throw new \Exception('Range does not intersect.'))
      : ($innerRange->period->isDuring($this->period) ?: throw new \Exception('Range does not fully intersect.'));
  }

  /**
   * Outputs a string useful for debugging.
   */
  public function __toString(): string {
    return \sprintf(
      '%s to %s',
      $this->period()->getStartDate()->format('r'),
      $this->period()->getEndDate()->format('r'),
    );
  }

}
