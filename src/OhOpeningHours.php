<?php

declare(strict_types=1);

namespace Drupal\oh;

use Drupal\Core\Entity\EntityInterface;
use Drupal\oh\Event\OhEvent;
use Drupal\oh\Event\OhEvents;
use Drupal\oh\Event\OhExceptionEvent;
use Drupal\oh\Event\OhProcessEvent;
use Drupal\oh\Event\OhRegularEvent;
use League\Period\Sequence;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Opening hours service.
 */
class OhOpeningHours implements OhOpeningHoursInterface {

  /**
   * Date format for a day in time.
   */
  const DAY_FORMAT = 'Y-m-d';

  /**
   * Constructs the opening hours service.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   An event dispatcher instance to use for configuration events.
   */
  public function __construct(
    protected EventDispatcherInterface $eventDispatcher,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getOccurrences(EntityInterface $entity, OhDateRange $range): array {
    $occurrences = $this->getDimensionalOccurrences($entity, $range);
    $event = new OhProcessEvent($entity, $range, $occurrences);
    $this->eventDispatcher->dispatch($event, OhEvents::PROCESS);
    return $event->getOccurrences();
  }

  /**
   * {@inheritdoc}
   */
  public function getDimensionalOccurrences(EntityInterface $entity, OhDateRange $range): array {
    $occurrences = $this->getRegularHours($entity, $range);
    $exceptions = $this->getExceptions($entity, $range);

    if ($exceptions) {
      foreach ($exceptions as $exception) {
        $dayKey = $exception->period()->getStartDate()->format(static::DAY_FORMAT);
        if (!$exception->getRegularHourInteraction() & OhOccurrence::REGULAR_HOUR_INTERACTION_VOID_REGULAR) {
          continue;
        }

        // Remove any regular hour occurrences on the same day as exceptions.
        $occurrences = \array_filter($occurrences, static function (OhOccurrence $occurrence) use ($dayKey) {
          // Note: Don't account for multi-day. Instead exception-event should
          // produce exceptions for each day.
          return $occurrence->period()->getStartDate()->format(static::DAY_FORMAT) !== $dayKey;
        });
      }

      // Exceptions must be added after above loop otherwise they will void
      // each other. For example: if multiple exceptions occur on the same day
      // only the last would survive.
      \array_push($occurrences, ...$exceptions);
    }

    return $occurrences;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegularHours(EntityInterface $entity, OhDateRange $range): array {
    $event = new OhRegularEvent($entity, $range);
    $this->eventDispatcher->dispatch($event, OhEvents::REGULAR);
    return $event->getRegularHours();
  }

  /**
   * {@inheritdoc}
   */
  public function getExceptions(EntityInterface $entity, OhDateRange $range): array {
    $event = new OhExceptionEvent($entity, $range);
    $this->eventDispatcher->dispatch($event, OhEvents::EXCEPTIONS);
    return $event->getExceptions();
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents(EntityInterface $entity, OhDateRange $range): array {
    $event = new OhEvent($entity, $range);
    $this->eventDispatcher->dispatch($event, OhEvents::EVENT);
    return $event->getEvents();
  }

  /**
   * {@inheritdoc}
   */
  public function getEventsWhileOpen(EntityInterface $entity, OhDateRange $range): array {
    $openingOccurrences = \array_filter(
      $this->getOccurrences($entity, $range),
      static fn (OhOccurrence $occurrence): bool => $occurrence->isOpen(),
    );
    $specialEvents = $this->getEvents($entity, $range);

    $openings = new Sequence();
    foreach ($openingOccurrences as $openingOccurrence) {
      $openings->push($openingOccurrence->period());
    }

    // Sanity check no intersections in $openings, this should be guaranteed by
    // getOccurrences, but we check again as bad computations would be made
    // if there were pre-existing intersections when doing special-hours
    // intersection calculation below.
    if (\count($openings->intersections()) > 0) {
      throw new \LogicException('getOccurrences should have guaranteed there are no period intersections');
    }

    $result = [];
    foreach ($specialEvents as $specialEvent) {
      // Since this is a shallow clone, it shouldn't impact perf too much.
      $openingsAndEvent = clone $openings;
      $openingsAndEvent[] = $specialEvent->period();
      foreach ($openingsAndEvent->intersections() as $intersection) {
        $result[] = (clone $specialEvent)->setPeriod($intersection);
      }
    }

    return $result;
  }

}
