<?php

declare(strict_types=1);

namespace Drupal\oh\EventSubscriber;

use Drupal\oh\Event\OhEvents;
use Drupal\oh\Event\OhProcessEvent;
use Drupal\oh\OhUtility;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines a event subscriber.
 */
final class OhEventSubscriber implements EventSubscriberInterface {

  /**
   * Records partial day messages.
   */
  private \WeakMap $partialDayMessages;

  /**
   * OhEventSubscriber constructor.
   */
  public function __construct() {
    $this->partialDayMessages = new \WeakMap();
  }

  /**
   * Gather full day occurrences.
   *
   * Records full day occurrences, so partial occurrence messages can be
   * removed.
   *
   * @param \Drupal\oh\Event\OhProcessEvent $event
   *   Process event.
   */
  public function gatherFullDay(OhProcessEvent $event): void {
    $occurrences = $event->getOccurrences();
    $messages = [];
    foreach ($occurrences as $occurrence) {
      if (!$occurrence->isFullDay()) {
        $day = $occurrence->period()->getStartDate()->format('Y-m-d');
        $messages[$day] = \array_merge($messages[$day] ?? [], $occurrence->getMessages());
      }
    }
    $this->partialDayMessages[$event] = $messages;
  }

  /**
   * Flattens occurrences.
   *
   * @param \Drupal\oh\Event\OhProcessEvent $event
   *   Process event.
   */
  public function flattenOccurrences(OhProcessEvent $event): void {
    $occurrences = $event->getOccurrences();
    $occurrences = OhUtility::flattenOccurrences($occurrences);
    $event->setOccurrences($occurrences);
  }

  /**
   * Reapply full day messages.
   *
   * Removes any messages acquired from partial day.
   *
   * @param \Drupal\oh\Event\OhProcessEvent $event
   *   Process event.
   */
  public function reapplyMessages(OhProcessEvent $event): void {
    /** @var string[] $partialDayMessages */
    $partialDayMessages = $this->partialDayMessages[$event];
    if (\count($partialDayMessages) === 0) {
      return;
    }

    $occurrences = $event->getOccurrences();
    foreach ($occurrences as $occurrence) {
      $day = $occurrence->period()->getStartDate()->format('Y-m-d');
      if ($occurrence->isFullDay()) {
        // Remove any partial day messages, leaving full day messages.
        $differences = \array_diff($occurrence->getMessages(), $partialDayMessages[$day] ?? []);
        $occurrence->setMessages(\array_values($differences));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[OhEvents::PROCESS][] = ['gatherFullDay', 1000];
    $events[OhEvents::PROCESS][] = ['flattenOccurrences'];
    $events[OhEvents::PROCESS][] = ['reapplyMessages', -1000];
    return $events;
  }

}
