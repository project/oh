<?php

declare(strict_types=1);

namespace Drupal\oh;

use Drupal\Core\Cache\CacheableMetadata;
use League\Period\Period;
use League\Period\Sequence;

/**
 * Provide standalone utilities assisting opening hours.
 */
class OhUtility {

  /**
   * Flattens occurrences so they do not overlap.
   *
   * Takes occurrences and flattens so that occurrences do not overlap. Some
   * occurrences will be broken up.
   *
   * If occurrences have messages, they will be broken up too. Handle
   * appropriately, such as deduplication, on the front end.
   *
   * @param \Drupal\oh\OhOccurrence[] $occurrences
   *   An array of occurrences to flatten.
   *
   * @return \Drupal\oh\OhOccurrence[]
   *   An array of occurrences whose ranges do not overlap.
   */
  public static function flattenOccurrences(array $occurrences): array {
    /** @var \WeakMap<\League\Period\Period, \Drupal\oh\OhOccurrence> $all */
    $all = new \WeakMap();
    $openings = new Sequence();
    $closures = new Sequence();
    foreach ($occurrences as $occurrence) {
      $period = $occurrence->period();
      $all[$period] = $occurrence;
      ($occurrence->isOpen() ? $openings : $closures)->push($period);
    }

    $openingsUnion = $openings->unions();
    $closuresUnion = $closures->unions();
    $newOpenings = $openingsUnion->subtract($closuresUnion);

    $newOccurrences = [];
    foreach ([
      [$newOpenings, $openings, TRUE],
      [$closuresUnion, $closures, FALSE],
    ] as [$union, $old, $isOpen]) {
      // Merge metadata (messages, cacheability, etc) from old, to new union.
      foreach ($union as $period) {
        \assert($period instanceof Period);
        $messages = [];
        $cacheability = new CacheableMetadata();
        foreach ($old as $innerPeriod) {
          \assert($innerPeriod instanceof Period);
          if ($period->overlaps($innerPeriod)) {
            $innerOccurrence = $all[$innerPeriod] ?? throw new \LogicException('Period should exist on this map as $all, $openings, $closures share the same keys.');
            $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($innerOccurrence));
            \array_push($messages, ...$innerOccurrence->getMessages());
          }
        }

        // If end time zone is different, make it match start, for sanity.
        if ($period->getStartDate()->getTimezone()->getName() !== $period->getEndDate()->getTimezone()->getName()) {
          $period = new Period(
            $period->getStartDate(),
            \DateTime::createFromInterface($period->getEndDate())->setTimezone($period->getStartDate()->getTimezone()),
          );
        }

        $newOccurrences[] = OhOccurrence::fromPeriod($period)
          ->setIsOpen($isOpen)
          ->addMessages(...$messages)
          ->addCacheableDependency($cacheability);
      }
    }

    \usort($newOccurrences, [OhDateRange::class, 'sort']);

    return $newOccurrences;
  }

}
